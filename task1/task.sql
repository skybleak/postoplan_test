SELECT u.username, COUNT(p.post_id) as posts_count
FROM `users` AS u
         LEFT JOIN user_plan AS up ON up.user_id = u.user_id
         LEFT JOIN users_meta AS um ON um.user_id = u.user_id
         LEFT JOIN posts AS p ON p.user_id = u.user_id
WHERE (up.plan_id = 19 AND up.posts_count > 5)
  AND (um.meta_name = 'user_language' AND um.meta_value IN ('english', 'spanish'))
GROUP BY u.user_id
