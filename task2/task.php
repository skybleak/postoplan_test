<?php

require 'Services\DB.php';

$start = microtime(true);

$import_file = $argv[1];
$export_file = $argv[2] ?? "task2.sql";
$database = $argv[3] ?? "test";
$table = basename($export_file, '.sql');

try {
    $countRow = processingCsv($import_file, $export_file, $database, $table, $dbh);
    $endTimeScript = round(microtime(true) - $start, 3);

    echo "Время выполнения скрипта: $endTimeScript sec. \nКоличество сохранённых строк: $countRow";
} catch (Exception $e) {
    echo $e->getMessage();
}


/*
|--------------------------------------------------------------------------
| Run initial scan of CSV file.
|--------------------------------------------------------------------------
*/
/**
 * @param string $import_file
 * @param string $export_file
 * @param string $database
 * @param string $table
 * @param PDO $dbh
 * @return int
 * @throws Exception
 */
function processingCsv(string $import_file, string $export_file, string $database, string $table, PDO $dbh): int
{
    if (($input = @fopen($import_file, 'r')) != false) {
        $row = 1;
        while (($fields = fgetcsv($input, 1000, ',')) != false) {
            if ($row == 1) {
                foreach ($fields as $field) {
                    $headers[] = strtolower(str_ireplace(' ', '_', $field));
                }
            } else {
                foreach ($fields as $key => $value) {
                    if (!isset($max_field_lengths[$key])) {
                        $max_field_lengths[$key] = 0;
                    }

                    if (strlen($value) > $max_field_lengths[$key]) {
                        $max_field_lengths[$key] = strlen($value);
                    }
                    $field++;
                }
            }
            $row++;
        }
        fclose($input);
    } else {
        echo 'Unable to open file "' . $import_file . '".' . "\n";
    }

    return generateSql($import_file, $export_file, $database, $table, $headers, $max_field_lengths, $dbh);
}

/*
|--------------------------------------------------------------------------
| Generate new importable SQL file.
|--------------------------------------------------------------------------
*/

/**
 * @param string $import_file
 * @param string $export_file
 * @param string $database
 * @param string $table
 * @param array $headers
 * @param array $max_field_lengths
 * @param PDO $dbh
 * @return int
 */
function generateSql(string $import_file, string $export_file, string $database, string $table, array $headers, array $max_field_lengths, PDO $dbh): int
{
    $output = fopen($export_file, 'w');
    fwrite($output, 'CREATE TABLE `' . $database . '`.`' . $table . '` (' . "\n");
    fwrite($output, '`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,' . "\n");

    foreach ($headers as $key => $header) {
        fwrite($output, '`' . $header . '` VARCHAR(' . $max_field_lengths[$key] . ') NOT NULL,' . "\n");
    }

    fwrite($output, 'PRIMARY KEY (`id`)' . "\n" . ') DEFAULT CHARACTER SET \'utf8\';' . "\n" . "\n");

    $sqlData = '';
    if (($input = @fopen($import_file, 'r')) != false) {
        $row = 1;

        $dbh->beginTransaction();
        while (($fields = fgetcsv($input, 1000, ',')) != false) {
            if (sizeof($fields) != sizeof($headers)) {
                echo 'INCORRECT NUMBER OF FIELDS  (search your file for \'\"\' string):';
                echo print_r($fields, true);
                die();
            }
            if ($row != 1) {
                $sql = 'INSERT INTO `' . $database . '`.`' . $table . '` VALUES(null, ';
                foreach ($fields as $field) {
                    $sql .= '\'' . htmlspecialchars(stripslashes(($field))) . '\', ';
                }
                $sql = rtrim($sql, ', ');
                $sql .= ');';
                fwrite($output, $sql . "\n");

                $dbh->exec($sql);
            }
            $row++;
        }
        $dbh->commit();

        fclose($input);
    } else {
        echo 'Unable to open file "' . $import_file . '".' . "\n";
    }
    fclose($output);

    return $row ?? 0;
}