<?php

namespace Services;

use Exception;
use PDO;
use PDOException;

$host = '127.0.0.1';
$dbname = 'test';
$user = 'root';
$password = '';


try {
    $dbh = new PDO("mysql:dbname=$dbname;host=$host", $user, $password);
} catch (PDOException $e) {
    die($e->getMessage());
}

